﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextScript : MonoBehaviour
{
    public string Text;
    public string[] SSPs;
    public Text text;
    // Start is called before the first frame update
    void Start()
    {
        text = gameObject.GetComponent<Text>();
        SSPs[0] = "In this Scenario we have grown to know a more sustainable lifestyle that betters our earth, but with this new lifestyle comes some rules that everybody has to follow. We can call these rules Environmental Limits as they put a cap on growth of materials and lowering the intensity of the energy that we use in our day to day lives. When adding these limits we put our energy into the well being of the planet by investing in our education and health which directly correlates to the lowering of inequality within all countries. By doing this we all have access to the resources for living an environmentally stable lifestyle.";
        SSPs[1] = "In this Scenario we extend our historical background with having inconsistencies in our historical, social, and economic trends. Everything we do from basic societal development to gaining revenue is quite unstable throughout the world with other countries doing better than others. Unfortunately we would be growing in income disparity and we would be adding more obstacles to live within a better society and environment.This would be due to the degradation of our environment in contrast with the energy we put out to make up for these inequalities in our growth.";
        SSPs[2] = "In this Scenario we become more divided in the world as nationalism is revived and countries focus on regional issues, this then makes the bond between other countries weaker which results in security concerns. As countries expand they put their focus on energy and food.  A decline in investment towards education and technical growth arises along with worsened disparities and a poor economic growth. Developing countries well have a strong growth in population while developed countries will experience a poor result. Since nations are focused on their own problems there will be a lower international priority over resolving environmental issues and some regions will experience extreme environmental degradation.";
        SSPs[3] = "In this Scenario there will be a large increase in inequality and stratification among all nations, this is due to unequal investments in human resources in conjunction with growing differences in economic opportunities.  A big gap will be created between an internationally linked society who would be contributing to the global economy and  disperse low income societies operating in a labor intensive economy.  The high tech economy and industries will drastically grow as social harmony degrades and the globally linked energy market will diversify as investments will be made in carbon intensive fuels and low carbon energy sources.";
        SSPs[4] = "In this scenario we overly depend on competitive markets and participatory communities to create a pathway to sustainable development using a rapid technological evolution. Our global markets become interconnected and human and social resources develop due to strong investments in health, education, and institutions. Economic and social growth are linear with our fossil fuel resources and energy intensive lifestyle, both of them contribute to the rapid growth in the global economy. Local environmental issues such as air pollution are handled effectively and we become more confident in our handling of future social and economic progress by handling geo-engineering.";
    }
    // Update is called once per frame
        void Update()
    {
        text.text = Text;
    }
}
