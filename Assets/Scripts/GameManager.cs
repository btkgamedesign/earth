﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public SpinFree Spin;
    public GameObject Text;
    public GameObject ExplanationText;
    public GameObject Info;
    // Start is called before the first frame update
    void Start()
    {
        //Text = GameObject.Find("Text");
        Text.SetActive(false);
        Info.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Equals))
        {
            Spin.speed++;
        }
        if (Input.GetKeyDown(KeyCode.Minus))
        {
            Spin.speed--;
        }
        if(Input.GetKeyDown(KeyCode.Space))
        {
            Spin.speed = 0;
        }
    }

    public void StartText()
    {
        Text.SetActive(true);
        ExplanationText.SetActive(false);
    }
    public void InfoText()
    {
        if(Info.activeSelf)
        {
            
            Info.SetActive(false);
        }
        else
        {
            Info.SetActive(true);
        }
    }
}
