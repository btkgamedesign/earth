﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextureController : MonoBehaviour
{
    public MeshRenderer earthShader;
    public float Opacity;

    public Slider TimeSlider;
    public float SliderValue;

    public float TimeIncrease;
    // Start is called before the first frame update
    void Start()
    {
        TimeSlider = GameObject.Find("Time").GetComponent<Slider>();
        SliderValue = TimeSlider.value;
        earthShader = gameObject.GetComponent<MeshRenderer>();
        Opacity = gameObject.GetComponent<MeshRenderer>().material.GetFloat("transition");
        Debug.Log(earthShader);
    }

    // Update is called once per frame
    void Update()
    {
        //SliderValue = TimeSlider.value;
        earthShader.material.SetFloat("transition", SliderValue);
        Debug.Log(earthShader);
        if (SliderValue <= 1)
        {
            SliderValue = SliderValue + (TimeIncrease/100);
        }
    }

 
}
