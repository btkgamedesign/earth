﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpriteChange : MonoBehaviour
{
    public Color col;
    public Slider polution;
    public float SliderValue;
    public float percentage;
    public void Start()
    {
        polution = GameObject.Find("Slider").GetComponent<Slider>();
        SliderValue = polution.value;
        
    }
    public void Update()
    {
        if (SliderValue <= percentage)
        { SliderValue = polution.value; }
        if(SliderValue >= percentage)
        {
            gameObject.GetComponent<Renderer>().material.color = new Color(SliderValue * percentage, SliderValue * percentage, SliderValue * percentage, SliderValue * percentage);
            SliderValue = 1 - polution.value;
        }
        // var col = gameObject.GetComponent<Renderer>().material.color;
        
        //gameObject.GetComponent<Renderer>().material.color = new Color(SliderValue/percentage, SliderValue / percentage, SliderValue / percentage, SliderValue / percentage);
        
    }


    
    
}
