﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Dial : MonoBehaviour
{
    public Color col;
    public Slider DailSlider;
    public float SliderValue;
   

    public Material[] materials;
    public int IterationNum;
    private float IterationValue;
    private float IterationRange;

    private float[] StartIterationValue;
    private float[] EndIterationValue;
    public TextScript textScript;

    //AudioScript
    private AudioSource audioSource;
    private AudioManager audioManager;
    public GameObject GameManager;
    public void Start()
    {
        
        //DailSlider = GameObject.Find("Slider").GetComponent<Slider>();
        //textScript = GameObject.Find("Text").GetComponent<TextScript>();
        SliderValue = DailSlider.value;
        //TopFloat = Top / 100f;
       // BottomFloat = Bottom / 100f;
        IterationValue = ((float)1 / (float)IterationNum);
        Debug.Log(IterationValue);
        IterationRange = IterationValue;
        StartIterationValue = new float[IterationNum];
        EndIterationValue = new float[IterationNum];
        //Audio
        //audioSource = GameManager.GetComponent<AudioSource>();
        
        Setup();
        
    }
    public void Update()
    {
      
        CheckPosition();      

    }

    public void Setup()
    {
        

        for (int i = 0; i <= IterationNum; i++)
        {
            StartIterationValue[i] = (float)i * (float)IterationValue;
            EndIterationValue[i] = (float)i * (float)IterationValue + IterationValue;
        }
    }

    public void CheckPosition()
    {
        SliderValue = DailSlider.value;
        for (int i = 0; i <= IterationNum; i++)
        {
            //Debug.Log(StartIterationValue[i]);
            //Debug.Log(EndIterationValue[i]);

            if (SliderValue >= StartIterationValue[i] && SliderValue <= EndIterationValue[i])
            {
                
                gameObject.GetComponent<Renderer>().material = materials[i];
                textScript.Text = textScript.SSPs[i];
                //AudioPlay
               // Debug.Log("Play Audio");
               // audioSource.clip = audioManager.Clips[i];
               //audioSource.Play();
            }
            else
            {

            }
        }
    }
}
